import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './pages/home/home.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { SigninComponent } from './pages/signin/signin.component';
import { SignupComponent } from './pages/signup/signup.component';
import { AdminComponent } from './pages/admin/admin.component';
import { CreateEventComponent } from './components/create-event/create-event.component';
import { FooterComponent } from './components/footer/footer.component';
import { SearchGroupComponent } from './components/search-group/search-group.component';
import { UpcomingEventsComponent } from './components/upcoming-events/upcoming-events.component';
import { CategoriesHomeComponent } from './components/categories-home/categories-home.component';
import { ButtonComponent } from './components/button/button.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { GroupComponent } from './components/group/group.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { CategoriesSelectComponent } from './components/categories-select/categories-select.component';
import { ShowGroupsAdminComponent } from './components/show-groups-admin/show-groups-admin.component';
import { ButtonAdminComponent } from './components/button-admin/button-admin.component';
import { EditGroupComponent } from './components/edit-group/edit-group.component';
import { ImageGroupComponent } from './components/image-group/image-group.component';
import { SpinnerComponent } from './components/spinner/spinner.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    SigninComponent,
    SignupComponent,
    AdminComponent,
    CreateEventComponent,
    FooterComponent,
    SearchGroupComponent,
    UpcomingEventsComponent,
    CategoriesHomeComponent,
    ButtonComponent,
    PageNotFoundComponent,
    GroupComponent,
    CategoriesComponent,
    CategoriesSelectComponent,
    ShowGroupsAdminComponent,
    ButtonAdminComponent,
    EditGroupComponent,
    ImageGroupComponent,
    SpinnerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    Title
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
