import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ResponseStatus } from 'src/app/helpers/api.response';
import { errorNotification, okNotification, successNotification } from 'src/app/helpers/swal.alert';
import { User } from 'src/app/interfaces/access';
import { SignupService } from '../../services/signup.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  title = 'Crea tu Cuenta'
  spinner: boolean = false;

  private isValidEmail = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
  signupForm = this.fb.group({
    email: ['', [Validators.required, Validators.pattern(this.isValidEmail)]],
    name: ['', [Validators.required, Validators.minLength(3)]],
    password: ['', [Validators.required, Validators.minLength(6)]],
    confirmPassword: ['', [Validators.required, Validators.minLength(6)]]
  })

  btnSignup: boolean = true;

  constructor(
    private titleService: Title,
    private signupService: SignupService,
    private router: Router,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.setTitle()
  }

  async signup(values) {
    try {
      this.btnSignup = true;
      this.spinner = true;
      const user = Object.assign({}, values);

      delete user.confirmPassword
      await this.signupService.createUser(user)
      okNotification('Cuenta creada correctamente', 'Confirma tu cuenta en tu correo para iniciar sesion (aun no funciona, pero funcionara :V)');
      this.router.navigateByUrl('/iniciar-sesion')
      return
    } catch (err) {
      errorNotification(err.error.message);
      this.btnSignup = false;
      this.spinner = false;
    }
  }

  formValid(): void {
    if (this.signupForm.valid) {
      this.btnSignup = false;
    } else {
      this.btnSignup = true;
    }
  }

  setTitle(): void {
    this.titleService.setTitle(`Workshops - Crea tu Cuenta`);
  }

  getErrorMessage(field: string): string {
    let message: string;
    if (this.signupForm.get(field).errors.required) {
      message = 'El campo es requerido';
    } else if (this.signupForm.get(field).hasError('pattern')) {
      message = 'Email invalido';
    } else if (this.signupForm.get(field).hasError('minlength')) {
      const minLength = this.signupForm.get(field).errors?.minlength.requiredLength;
      message = `este campo debe ser de minimo ${minLength} caracteres`;
    }
    return message;
  }

  checkField(field: string): boolean {
    const validateCheck = (this.signupForm.get(field).touched || this.signupForm.get(field).dirty) && !this.signupForm.get(field).valid;
    return validateCheck;
  }

}
