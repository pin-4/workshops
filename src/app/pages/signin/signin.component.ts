import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { ResponseStatus } from 'src/app/helpers/api.response';
import { errorNotification, successNotification } from 'src/app/helpers/swal.alert';
import { Signin } from 'src/app/interfaces/access';
import { SigninService } from '../../services/signin.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  user: Signin
  spinner: boolean = false;

  private isValidEmail = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
  signinForm = this.fb.group({
    email: ['', [Validators.required, Validators.pattern(this.isValidEmail)]],
    password: ['', [Validators.required, Validators.minLength(6)]]
  })

  btnSignin: boolean = true;

  constructor(
    private titleService: Title,
    private signinService: SigninService,
    private router: Router,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.setTitle()
  }

  async signin(values) {
    try {
      this.btnSignin = true;
      this.spinner = true;
      const result = await this.signinService.signin(values);
      this.user = result.data;
      window.localStorage.setItem('token', this.user.tokens)
      successNotification(`Hi ${this.user.user.nombre}`, result.message)
      this.router.navigateByUrl('/admin')
      return
    } catch (err) {
      errorNotification(err.error.message);
      this.btnSignin = false;
      this.spinner = false;
    }
  }

  setTitle(): void {
    this.titleService.setTitle(`Workshops - Inicia Session`);
  }

  formValid(): void {
    if (this.signinForm.valid) {
      this.btnSignin = false;
    } else {
      this.btnSignin = true;
    }
  }

  getErrorMessage(field: string): string {
    let message: string;
    if (this.signinForm.get(field).errors.required) {
      message = 'El campo es requerido';
    } else if (this.signinForm.get(field).hasError('pattern')) {
      message = 'Email invalido';
    } else if (this.signinForm.get(field).hasError('minlength')) {
      const minLength = this.signinForm.get(field).errors?.minlength.requiredLength;
      message = `este campo debe ser de minimo ${minLength} caracteres`;
    }
    return message;
  }

  checkField(field: string): boolean {
    const validateCheck = (this.signinForm.get(field).touched || this.signinForm.get(field).dirty) && !this.signinForm.get(field).valid;
    return validateCheck;
  }
}
