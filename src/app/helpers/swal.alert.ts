
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { DeleteNotification } from '../interfaces/admin';

export const successNotification = (title: string, message: string) => {
  Swal.fire({
    position: 'top-end',
    icon: 'success',
    title: title,
    text: message,
    showConfirmButton: false,
    timer: 2000
  })
}

export const okNotification = (title: string, message: string) => {
  Swal.fire(title, message, 'success');
}

export const errorNotification = (message: string) => {
  Swal.fire({
    icon: 'error',
    title: 'Oops...',
    text: message
  })
}

export const deleteNotification = (options: DeleteNotification): Promise<boolean> => new Promise((resolve, reject) => {
  const { id, title, text, _delete } = options;
  Swal.fire({
    title,
    text,
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, borrar!',
    cancelButtonText: 'Cancelar'
  }).then((result) => {
    if (result.value) {
      _delete.delete(id).then(result => {
        Swal.fire(
          'Eliminado',
          `${result.message}`,
          'success'
        )
        resolve(true);
      }).catch(error => {
        Swal.fire(
          'Error',
          `${error.message}`,
          'Error'
        )
        resolve(false);
      })
    }
  })
})
