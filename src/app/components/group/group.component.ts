import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { errorNotification, successNotification } from 'src/app/helpers/swal.alert';
import { GroupsService } from 'src/app/services/groups.service';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss']
})
export class GroupComponent implements OnInit {
  title = 'Crear grupo'
  spinner: boolean = false;
  imageError: string;
  isImageSaved: boolean;
  cardImageBase64: string;

  private isValidUrl = /^(ftp|http|https):\/\/[^ "]+$/;
  groupForm = this.fb.group({
    name: ['', [Validators.required, Validators.minLength(3)]],
    description: ['', [Validators.required, Validators.minLength(3)]],
    categoryId: ['', [Validators.required, Validators.minLength(1)]],
    image: [''],
    url: ['', [Validators.pattern(this.isValidUrl)]]
  })

  btnGroup: boolean = true;

  constructor(
    private fb: FormBuilder,
    private groupsService: GroupsService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  group(values) {
    this.btnGroup = true;
    this.spinner = true;
    const group = Object.assign({}, values, { image: this.cardImageBase64 })
    this.groupsService.create(group).then(result => {
      successNotification('Grupo', result.message);
      this.btnGroup = false;
      this.spinner = false;
      this.router.navigateByUrl('/admin')
    }).catch(err => {
      this.btnGroup = false;
      this.spinner = false;
      errorNotification(err.message)
    })
  }

  formValid(): void {
    if (this.groupForm.valid) {
      this.btnGroup = false;
    } else {
      this.btnGroup = true;
    }
  }

  fileChangeEvent(fileInput: any) {
    this.imageError = null;
    if (fileInput.target.files && fileInput.target.files[0]) {
      // Size Filter Bytes
      const max_size = 20971520;
      const allowed_types = ['image/png', 'image/jpeg'];

      if (fileInput.target.files[0].size > max_size) {
        this.imageError =
          'Maximum size allowed is ' + max_size / 1000 + 'Mb';

        return false;
      }

      /*if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
        this.imageError = 'Only Images are allowed ( JPG | PNG )';
        return false;
      }*/
      const reader = new FileReader();
      reader.onload = (e: any) => {
        const image = new Image();
        image.src = e.target.result;
        image.onload = rs => {
          const img_height = rs.currentTarget['height'];
          const img_width = rs.currentTarget['width'];

          // console.log(img_height, img_width);

          const imgBase64Path = e.target.result;
          this.cardImageBase64 = imgBase64Path;
          this.isImageSaved = true;
          // this.previewImagePath = imgBase64Path;
        };
      };

      reader.readAsDataURL(fileInput.target.files[0]);
    }
  }

  getErrorMessage(field: string): string {
    let message: string;
    if (this.groupForm.get(field).errors.required) {
      message = 'El campo es requerido';
    } else if (this.groupForm.get(field).hasError('pattern')) {
      message = 'Url invalida example (http://www.google.com)';
    } else if (this.groupForm.get(field).hasError('minlength')) {
      const minLength = this.groupForm.get(field).errors?.minlength.requiredLength;
      message = `este campo debe ser de minimo ${minLength} caracteres`;
    }
    return message;
  }

  checkField(field: string): boolean {
    const validateCheck = (this.groupForm.get(field).touched || this.groupForm.get(field).dirty) && !this.groupForm.get(field).valid;
    return validateCheck;
  }
}
