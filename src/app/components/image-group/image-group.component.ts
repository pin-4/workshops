import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { baseUrl } from 'src/environments/api.url';
import { Groups } from 'src/app/interfaces/admin';
import { GroupsService } from 'src/app/services/groups.service';

@Component({
  selector: 'app-image-group',
  templateUrl: './image-group.component.html',
  styleUrls: ['./image-group.component.scss']
})
export class ImageGroupComponent implements OnInit {
  private sub: any;
  title = 'Imagen Grupo';
  group: Groups;
  id: string;
  image: string;

  constructor(
    private groupsService: GroupsService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(async (params) => {
      this.id = params['id'];
      await this.getGroup(this.id);
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  async getGroup(id: string) {
    try {
      const result = await this.groupsService.findOne(id)
      this.group = result.data;
      this.image = `${baseUrl}/groups/${this.group.imagen}`;
    } catch (error) {
      this.router.navigateByUrl('not-found')
    }
  }

}
