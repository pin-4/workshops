import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { deleteNotification } from 'src/app/helpers/swal.alert'

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Output() clickOnDelete = new EventEmitter<boolean>();

  @Input() name: string;
  @Input() class: string;
  @Input() link: string;
  @Input() _delete: string;
  @Input() options: any;

  constructor(
  ) { }

  ngOnInit(): void {
  }

  addNewItem(value: boolean) {
    this.clickOnDelete.emit(value);
  }

  async onDelete(event) {
    event.preventDefault();
    const options = Object.assign(this.options, { id: this._delete })
    const result = await deleteNotification(options)
    this.clickOnDelete.emit(result);
  }
}
