import { Component, OnInit } from '@angular/core';
import { DeleteNotification, Groups } from 'src/app/interfaces/admin';
import { GroupsService } from 'src/app/services/groups.service';

@Component({
  selector: 'app-show-groups-admin',
  templateUrl: './show-groups-admin.component.html',
  styleUrls: ['./show-groups-admin.component.scss']
})
export class ShowGroupsAdminComponent implements OnInit {
  spinner: boolean = true;
  groups: boolean | Groups[]

  options: DeleteNotification = {
    title: '¿Eliminar Grupo?',
    text: 'Un grupo eliminado no se puede recuperar',
    _delete: this.groupsService
  }

  constructor(
    private groupsService: GroupsService
  ) { }

  async ngOnInit() {
    await this.getGroups()
  }

  async getGroups() {
    const result = await this.groupsService.findAll()
    this.spinner = false;
    this.groups = result.data.length > 0 ? result.data : false;
  }
}
