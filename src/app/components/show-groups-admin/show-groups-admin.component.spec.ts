import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowGroupsAdminComponent } from './show-groups-admin.component';

describe('ShowGroupsAdminComponent', () => {
  let component: ShowGroupsAdminComponent;
  let fixture: ComponentFixture<ShowGroupsAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowGroupsAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowGroupsAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
