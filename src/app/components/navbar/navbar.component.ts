import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ResponseStatus } from 'src/app/helpers/api.response';
import { ValidTokenService } from 'src/app/services/valid-token.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  auth: string = window.localStorage.getItem('token')
  // auth: any

  constructor(
    private validTokenService: ValidTokenService,
    private router: Router
  ) { }

  ngOnInit() {
    // this.validToken()
  }

  signOff(){
    window.localStorage.removeItem('token')
    this.router.navigateByUrl('/iniciar-sesion')
  }

  /* async validToken() {
    this.auth = await this.validTokenService.session(this.token)
  } */

}
