import { Component, Input, OnInit } from '@angular/core';
import { Categories } from 'src/app/interfaces/admin';
import { CategoriesService } from 'src/app/services/categories.service';

@Component({
  selector: 'app-categories-select',
  templateUrl: './categories-select.component.html',
  styleUrls: ['./categories-select.component.scss']
})
export class CategoriesSelectComponent implements OnInit {
  categories: Categories

  @Input()
  groupForm: any

  constructor(
    private categoriesService: CategoriesService,
  ) { }

  async ngOnInit() {
    await this.getCategories()
  }

  async getCategories() {
    const result = await this.categoriesService.categories()
    this.categories = result.data
  }

}
