import { Component, OnInit } from '@angular/core';
import { Categories } from 'src/app/interfaces/admin';
import { CategoriesService } from 'src/app/services/categories.service';

@Component({
  selector: 'app-categories-home',
  templateUrl: './categories-home.component.html',
  styleUrls: ['./categories-home.component.scss']
})
export class CategoriesHomeComponent implements OnInit {
  categories: Categories
  spinner: boolean = true;

  constructor(
    private categoriesService: CategoriesService
  ) { }

  async ngOnInit() {
    await this.getCategories()
  }

  async getCategories() {
    const result = await this.categoriesService.categories()
    this.spinner = false;
    this.categories = result.data;
  }

}
