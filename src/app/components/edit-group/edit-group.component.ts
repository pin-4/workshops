import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { GroupsService } from 'src/app/services/groups.service';
import { Groups } from 'src/app/interfaces/admin';
import { errorNotification, successNotification } from 'src/app/helpers/swal.alert';

@Component({
  selector: 'app-edit-group',
  templateUrl: './edit-group.component.html',
  styleUrls: ['./edit-group.component.scss']
})
export class EditGroupComponent implements OnInit {
  title = 'Editar Grupo';
  spinner: boolean = true;
  group: Groups;
  id: string;
  btnEdit: boolean = false;
  private sub: any;

  groupForm = this.fb.group({
    name: [''],
    description: [''],
    categoryId: [''],
    url: ['']
  })

  constructor(
    private fb: FormBuilder,
    private groupsService: GroupsService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(async (params) => {
      this.id = params['id'];
      await this.getGroup(this.id);
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  async getGroup(id: string) {
    try {
      const result = await this.groupsService.findOne(id)
      this.spinner = false;
      this.group = result.data;
      this.groupForm.setValue({
        name: this.group.nombre,
        description: this.group.descripcion,
        categoryId: this.group.categoriaid,
        url: this.group.url || ''
      })
    } catch (error) {
      this.router.navigateByUrl('not-found')
    }
  }

  editGroup(values) {
    this.btnEdit = true;
    this.spinner = true;
    this.groupsService.update(values, this.group.id).then(result => {
      successNotification('Grupo', result.message)
      this.router.navigateByUrl('/admin')
    }).catch(err => {
      this.spinner = false;
      this.btnEdit = false;
      errorNotification(err.message)
    })
  }

}
