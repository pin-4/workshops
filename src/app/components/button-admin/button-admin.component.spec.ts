import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonAdminComponent } from './button-admin.component';

describe('ButtonAdminComponent', () => {
  let component: ButtonAdminComponent;
  let fixture: ComponentFixture<ButtonAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
