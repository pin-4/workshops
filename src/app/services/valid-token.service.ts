import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { api } from 'src/environments/api.url';
import { ResponseStatus } from 'src/app/helpers/api.response'
import { headers } from '../helpers/headers';

@Injectable({
  providedIn: 'root'
})
export class ValidTokenService {

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  request(): Promise<any> {
    return new Promise((resolve, rejects) => {
      this.http.get(api.session, headers())
        .subscribe(result => {
          resolve(result)
        },
          err => rejects(err)
        )
    })
  }

  async session() {
    try {
      const auth = await this.request()
      return true
    } catch (err) {
      if (err.status === ResponseStatus.UNAUTHORIZED) {
        console.log(err.error.message)
        this.router.navigateByUrl('/iniciar-sesion')
        return false
      }
      console.log(err)
    }
  }
}
