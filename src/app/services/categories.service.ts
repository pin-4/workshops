import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { api } from 'src/environments/api.url';
import { headers } from '../helpers/headers';
import { invalidToken } from '../helpers/invalid.token';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  categories(): Promise<any> {
    return new Promise((resolve, _rejects) => {
      this.http.get(api.categories, headers())
        .subscribe(result => {
          resolve(result)
        },
          err => invalidToken(err, this.router)
        )
    })
  }
}
