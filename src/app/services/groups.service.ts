import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { api } from 'src/environments/api.url';
import { ResponseStatus } from '../helpers/api.response';
import { headers } from '../helpers/headers';
import { invalidToken } from '../helpers/invalid.token';
import { Groups } from '../interfaces/admin';

@Injectable({
  providedIn: 'root'
})
export class GroupsService {

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  create(group: Groups): Promise<any> {
    return new Promise((resolve, rejects) => {
      this.http.post(api.groups.create, group, headers())
        .subscribe(result => {
          resolve(result)
        },
          err => {
            if (err.status === ResponseStatus.UNAUTHORIZED) return invalidToken(err, this.router)
            rejects({
              message: err.error.message,
              status: err.status
            })
          }
        )
    })
  }

  findAll(): Promise<any> {
    return new Promise((resolve, rejects) => {
      this.http.get(api.groups.findAll, headers())
        .subscribe(result => {
          resolve(result)
        },
          err => {
            if (err.status === ResponseStatus.UNAUTHORIZED) return invalidToken(err, this.router)
            rejects({
              message: err.error.message,
              status: err.status
            })
          }
        )
    })
  }

  delete(id: string): Promise<any> {
    return new Promise((resolve, rejects) => {
      this.http.delete(api.groups.delete(id), headers())
        .subscribe(result => {
          resolve(result)
        },
          err => {
            if (err.status === ResponseStatus.UNAUTHORIZED) return invalidToken(err, this.router)
            rejects({
              message: err.error.message,
              status: err.status
            })
          }
        )
    })
  }

  findOne(id: string): Promise<any> {
    return new Promise((resolve, rejects) => {
      this.http.get(api.groups.findOne(id), headers())
        .subscribe(result => {
          resolve(result)
        },
          err => {
            if (err.status === ResponseStatus.UNAUTHORIZED) return invalidToken(err, this.router)
            rejects({
              message: err.error.message,
              status: err.status
            })
          }
        )
    })
  }

  update(group: Groups, id: string): Promise<any> {
    return new Promise((resolve, rejects) => {
      this.http.put(api.groups.update(id), group, headers())
        .subscribe(result => {
          resolve(result)
        },
          err => {
            if (err.status === ResponseStatus.UNAUTHORIZED) return invalidToken(err, this.router)
            rejects({
              message: err.error.message,
              status: err.status
            })
          }
        )
    })
  }
}
