import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { User } from '../interfaces/access';
import { api } from 'src/environments/api.url';

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  constructor(private http: HttpClient) { }

  createUser(user: User): Promise<any> {
    return new Promise((resolve, rejects) => {
      this.http.post(api.signup, user)
        .subscribe(result => {
          resolve(result)
        },
          err => rejects(err)
        )
    })
  }
}
