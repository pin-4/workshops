import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { EditGroupComponent } from './components/edit-group/edit-group.component'
import { GroupComponent } from './components/group/group.component'
import { ImageGroupComponent } from './components/image-group/image-group.component'
import { AuthService } from './middlewares/auth.service'
import { AdminComponent } from './pages/admin/admin.component'
import { HomeComponent } from './pages/home/home.component'
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component'
import { SigninComponent } from './pages/signin/signin.component'
import { SignupComponent } from './pages/signup/signup.component'

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'iniciar-sesion', component: SigninComponent },
  { path: 'crear-cuenta', component: SignupComponent },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthService]
    /* children: [
      {
        path: 'crear-grupo',
        component: CreateGroupComponent
      }
    ] */
  },
  {
    path: 'admin/crear-grupo',
    component: GroupComponent
  },
  {
    path: 'admin/editar-grupo/:id',
    component: EditGroupComponent
  },
  {
    path: 'admin/editar-imagen-grupo/:id',
    component: ImageGroupComponent
  },
  { path: 'not-found', component: PageNotFoundComponent },
  { path: '**', redirectTo: 'not-found' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
