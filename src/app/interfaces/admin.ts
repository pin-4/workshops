export interface Categories {
  id: number,
  nombre: string,
  slug: string
}

export interface Users {
  id: number;
  nombre: string;
  imagen?: any;
  descripcion?: any;
  email: string;
}

export interface Groups {
  id: string;
  nombre: string;
  descripcion: string;
  url?: any;
  imagen: string;
  categoria: Categories;
  usuario: Users;
  categoriaid: number
}

export interface DeleteNotification {
  id?: string,
  title: string,
  text: string,
  _delete: any
}
