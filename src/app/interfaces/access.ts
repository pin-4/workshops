export interface User {
  id?: number;
  email: string;
  name: string;
  nombre?: string;
  password?: string;
}

export interface Signin {
  user: User;
  tokens: string;
}
